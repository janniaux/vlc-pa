from threading import Thread, Lock
from queue import Queue, Empty
import argparse, textwrap
from aiohttp import web, WSMsgType
import asyncio
import json
import sys
import webbrowser
import logging as log

class VLCLogParser:
    def __init__(self, queue, labels):
        self.queue = queue
        self.labels = labels

    def get_json_labels(self, data, id_label, id_number):
        config_label = ""

        if "type" in data:
            config_label += "[" + data["type"] + "]"
        if "stream" in data:
            config_label += "[" + data["stream"] + "]"
        label = config_label
        if "id" in data:
            config_label += "[" + id_label + "]"

            label += "[" + id_label
            if id_number != -1 :
                label += "/" + id_number
            label += "]"

        for l in self.labels:
            if l == config_label:
                return label, config_label
        return None, None

    def parse_json_log_line(self, line):
         # Parse the json line
        try:
            line_data = json.loads(line)
        except Exception as e:
            log.warning("Cannot parse json log line: " + str(e))
            return

        if "Timestamp" not in line_data or "Body" not in line_data:
            log.warning("Cannot find Timestamp or Body: " + line)
            return

        ts = line_data["Timestamp"]
        body = line_data["Body"]

        # Split the id (audio/1 in [audio, 1])
        if "id" in body:
            id_chunk = body["id"].split("/")
            id_label = id_chunk[0].upper()
            id_number = -1
            if (len(id_chunk) > 1):
                id_number = id_chunk[1]
        else:
            log.warning("No id in the trace: " + line)

        # label is the complete label of the data. Ex: [DMX][AUDIO/2][IN]
        # config_label is the label associated with in the config_file. Ex: [DMX][AUDIO][IN]
        label, config_label = self.get_json_labels(body, id_label, id_number)
        if label is None or config_label is None:
            log.warning("Cannot generate a valid label : " + str(body))
            return

        values = body

        queue.put((label, config_label, ts, values))
        log.debug("PUSHING" + str((label, config_label, ts, values)))

    def get_label(self, line):
        for label in self.labels:
            if label in line:
                return label
        return None

    def parse_log_line(self, line):
        # Drop line if it's not an avstat log
        if "avstat" not in line:
            return
        # find a label in line
        label = self.get_label(line)
        if label is None:
            log.warning("Cannot find a proper label in avstats: " + line)
            return
        log.debug("STATLINE: " + line)
        values = {}
        for chunk in line.replace(',','').split():
            if len(chunk.split("=")) == 2:
                k, v = chunk.split("=")
                try:
                    values[k] = int(v) / 10 ** 6
                except ValueError:
                    log.warning("Error: some timestamp or value are not convertible to integer: " + chunk)
        # "ts" aka system timestamp is mandatory
        if "ts" not in values.keys():
            log.warning(" - could not find timestamp for log: " + line)
            return
        ts = values.pop("ts")
        self.queue.put( (label, label, ts, values) )
        log.debug("PUSHING " + str((label, label, ts, values)))

class InputReaderThread(Thread):
    def __init__(self, parser):
        super(InputReaderThread, self).__init__()
        self.parser = parser

    def run(self):
        for line in sys.stdin:
            self.parser.parse_json_log_line(line)

class HTTPServerThread(Thread):
    def __init__(self, parser, port):
        super(HTTPServerThread, self).__init__()
        self.parser = parser
        self.port = port

    def aiohttp_server(self):
        async def handle_vlc_put(request):
            while True:
                body = await request.content.readline()
                if body == b'':
                    log.info(" --- end of logging")
                    break
                self.parser.parse_json_log_line(body.decode("utf-8"))
            return web.Response(text="")
        async def handle_index(request):
            return web.FileResponse("./static/index.html")
        async def handle_get_config(request):
            return web.FileResponse("./config.json")
        async def handle_websocket(request):
            log.info('Websocket connection starting')
            ws = web.WebSocketResponse()
            await ws.prepare(request)
            log.info('Websocket connection ready')
            async def read_msg(ws):
                async for msg in ws:
                    if msg.type == WSMsgType.TEXT:
                        if msg.data == 'close':
                            await ws.close()
            async def write_msg(ws):
                while True:
                    try:
                        # Do not block the async event loop
                        ( label, config_label, x, values_dict ) = queue.get_nowait()
                        values_dict["label"] = label
                        values_dict["config_label"] = config_label
                        values_dict["timestamp"] = x
                        await ws.send_str(json.dumps(values_dict))
                        queue.task_done()
                    except Empty:
                        # let other tasks run
                        await asyncio.sleep(0.1)
            read_task = asyncio.create_task(read_msg(ws))
            write_task = asyncio.create_task(write_msg(ws))
            await asyncio.gather(read_task, write_task)
            log.info('Websocket connection closed')
            return ws
        app = web.Application()
        app.add_routes([web.put('/', handle_vlc_put)])
        app.add_routes([web.get('/', handle_index)])
        app.add_routes([web.get('/config.json', handle_get_config)])
        app.add_routes([web.get('/ws', handle_websocket)])
        app.add_routes([web.static('/js', './static/js')])
        runner = web.AppRunner(app)
        return runner

    def run(self):
        # Run aiohttp in a separate thread, to let mathplotlib alone in the main one.
        runner = self.aiohttp_server()
        loop = asyncio.new_event_loop()
        loop.run_until_complete(runner.setup())
        site = web.TCPSite(runner, '0.0.0.0', self.port)
        loop.run_until_complete(site.start())
        loop.run_forever()

if __name__ == '__main__':
    # Parse arguments
    args_parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
        description=textwrap.dedent("""\
            Generate interactive plots based on VLC stat logs.

            Logs will be read from either stdin (default) or HTTP stream."""))
    args_parser.add_argument('--http', action='store_true', help="Listen to the incoming logs from the HTTP server instead of stdin")
    args_parser.add_argument('--port', type=int, default=8080, help="HTTP server listen port")
    args_parser.add_argument('--log', type=str, default="INFO", help="Log Verbosity")
    args = args_parser.parse_args()
    # Configure logging
    num_log_level = getattr(log, args.log.upper(), None)
    if not isinstance(num_log_level, int):
        raise ValueError('Invalid log level: %s' % num_log_level)
    log.basicConfig(level=num_log_level)
    # load config json file
    config = None
    with open("config.json", "r") as f:
        config = json.load(f)
    # Queue will be used as a FIFO between log parsing and Data plot
    queue = Queue()
    parser = VLCLogParser(queue, [ r[0] for r in config["resources"] ])
    reader_thread = None
    if not args.http:
        reader_thread = InputReaderThread(parser)
        reader_thread.start()
    reader_thread = HTTPServerThread(parser, args.port)
    reader_thread.start()
    webbrowser.open("http://localhost:{}".format(args.port))
    reader_thread.join()
